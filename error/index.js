module.exports = {
    COLLECTIVE_NOT_FOUND: "Collective not found",
    GET_COLLECTIVE_ERROR: "Error while fetching collective query",
    INFOBANK_NOT_FOUND: "Infobank not found",
    GET_INFORBANK_ERROR: "Error while fetching infobank query or Rss feed"
}