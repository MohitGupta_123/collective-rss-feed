const { db } = require('../config/index');
const knex = require("knex")(db);

module.exports = knex;