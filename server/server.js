
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('../config');

// Middleware
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// test route
app.get('/test', (req, res) => {
  console.log('I am config', config);
  res.send('Hello World! adfsdf')
})

app.use('/api', require('./api/api'))

//handle error
app.use(function(err, req, res, next) {
  // if error thrown from jwt validation check
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('Invalid token');
    return;
  }
  res.status(500).send(err.message);
});


module.exports = app;