const router = require('express').Router();
const controller = require('./controller');

router.param('collective_slug', controller.getCollective);

router.route('/:collective_slug/:infobank_slug')
    .get(controller.get)

module.exports = router;