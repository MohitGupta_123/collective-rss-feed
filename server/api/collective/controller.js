const db = require('../../../db/db');
const { COLLECTIVE_NOT_FOUND, GET_COLLECTIVE_ERROR, INFOBANK_NOT_FOUND, GET_INFORBANK_ERROR } = require('../../../error');
const feedConfig = require("../../../config").feed;
var RSS = require('rss');
var xml = require('xml');

module.exports.getCollective = async function (req, resp, next, collectiveSlug) {
    try {
        let meta_schema = await db('_meta_schema').select('meta_id', 'meta_type').where('id', db('_entity').select('meta_schema_id').where('slug', collectiveSlug)).first();
        // check if meta_scheam is for collective
        if (!meta_schema || meta_schema.meta_id !== 'collective' || meta_schema.meta_type != "entity") {
            const { COLLECTIVE_NOT_FOUND } = require('../../../error');
            return resp.status(404).json({ msg: COLLECTIVE_NOT_FOUND });
        }
        req.collective = await db('_entity').select().where('slug', collectiveSlug).first();
        // return resp.json(req.collective);
        next();
    } catch (err) {
        console.log(err);
        next(new Error(GET_COLLECTIVE_ERROR));
    }
}

module.exports.get = async function (req, resp, next) {
    let offset = req.query.page > 1 ?  req.query.page : 1;
    try {
        let infoBanks = await db.select('en.name as en_name', 'en.description as en_description','en.slug as en_slug', 'en.logo as en_logo', 'en.properties as en_properties', 'en.created_at as en_created', 'm.description as meta_description', 'm.properties as meta_properties').from('published_entity as pe')
            .innerJoin('_entity as en', 'en.id', 'pe.entity_id')
            .innerJoin('_meta_schema as m', 'm.id', 'en.meta_schema_id')
            .where('pe.parent_id', req.collective.id)
            .where('m.meta_id', req.params.infobank_slug)
            .where('pe.status', 1)
            .where('pe.state', 4)
            .where('en.visibility', 1)
            .where('en.status', 1)
            .where('en.access_type', 1)
            .where('en.state', 4)
            .limit(feedConfig.limit)
            .offset(offset);


        if (!infoBanks || !infoBanks.length) {
            return resp.status(404).json({ msg: INFOBANK_NOT_FOUND });
        }

        // Generate Rss Feed
        let feed = new RSS({
            title: req.collective.properties.name ? req.collective.properties.name : req.collective.properties.title,
            description: req.collective.properties.description,
            site_url: `${feedConfig.canonical_url_domain}${req.collective.slug}`,
            image_url: getImage(req.collective.properties)
        });

        // Add item to Feed
        for (let infoBank of infoBanks) {
            let title = getTitle(infoBank.en_properties);
            let publishDate = infoBank.en_created ? new Date(infoBank.en_created).toISOString() : "";
            let description = infoBank.en_properties.description ? infoBank.en_properties.description : "";
            let canonicalUrl = `${feedConfig.canonical_url_domain}${req.collective.slug}/${infoBank.en_slug}`;
            
            let encodedHtmlTem = getBody(infoBank.en_properties); // To-do
            let author = "abcde"; // To-do

            feed.item({
                title: title,
                description: description,
                url: canonicalUrl,
                custom_elements: [
                    {
                        "content:encoded": {
                            _cdata: encodedHtmlTem
                        }
                    },
                    {
                        "author": author
                    },
                    {
                        "pubDate": publishDate
                    }
                    
                ]
            })
        }

        var result = feed.xml();
        resp.set('Content-Type', 'text/xml');
        return resp.send(result);

    } catch (err) {
        console.log(err);
        next(new Error(GET_INFORBANK_ERROR))
    }

}


function getImage(properties) {
    if(properties.coverimage) return properties.coverimage;
    if(properties.covermedia) return properties.covermedia;
    if(properties.profileimage) return properties.profileimage;

    return "";
}

function getTitle(properties) {
    if(properties.title) return properties.title;
    if(properties.name) return properties.name;
    if(properties.content) return properties.content;

    return "";
}

function getBody(properties) {
    if(properties.body && properties.body.content) return properties.body.content;
    
    return "";
}