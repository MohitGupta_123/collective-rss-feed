const router = require('express').Router();

router.use('/', require('./collective/routes'));

module.exports = router;