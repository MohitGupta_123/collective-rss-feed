module.exports = Object.freeze({
    db: {
        client: "mysql2",
        connection: {
            host: "localhost",
            database: "devcollective",
            user: "root",
            password: "password",
            charset: "utf8mb4"
        },
        migrations: {
            directory: "db/migrations"
        },
        seeds: {
            directory: "db/seeds"
        }
    },
    feed: {
        canonical_url_domain: "https://localhost:4200/",
        limit: 50
    }
})