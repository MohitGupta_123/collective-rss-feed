module.exports = Object.freeze({
    db: {
        client: "mysql2",
        connection: {
            host: "localhost",
            database: "devcollective",
            user: "root",
            password: "password",
            charset: "utf8mb4"
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    }
})