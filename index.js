const app = require('./server/server');
const { db } = require('./config/index');
const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 3000;
const knex = require('./db/db');

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
})